#!/bin/bash

# remove all autogenerated files

for filepath in $(find -name "*.autogen.*")
do
	rm $filepath
done

#Generate enum classes from .enum files

for filepath in $(find -name "*.enum")
do
	lua tools/generate_enum.lua "$filepath"
done

# Update build files

./update_src_build_files
