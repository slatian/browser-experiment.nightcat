public class Night.Backend.HostResolver.Multiplexer : Night.Interface.Request.NameResolver, Object {
	
	Night.Interface.ResolverConfiguration.Host.Query resolver_configuration;
	
	public Multiplexer(Night.Interface.ResolverConfiguration.Host.Query configuration){
		this.resolver_configuration = configuration;
	}
	
	
	  ///////////////////////////////////////////
	 //  Night.Interface.Request.NameResolver //
	///////////////////////////////////////////
	
	public Night.Interface.Request.NameResolverResult lookup(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.resolver.host_multiplexing_resolver", Night.Interface.Request.ContextRole.HOST_RESOLVER);
		var configuration = context.get_my_configuration();
		string? scheme = configuration.get_value("scheme");
		string? host = configuration.get_value("hostname");
		string? port = configuration.get_value("port");
		if (scheme == null) {
			scheme = "";
		}
		if (host == null) {
			context.close_because_of_error("error.no_host_specified");
			return Night.Backend.NopWorker.ResolverResult.instance;
		}
		if (port == null) {
			context.close_because_of_error("error.no_port_specified");
			return Night.Backend.NopWorker.ResolverResult.instance;
		}
		var multiplexer = new Night.Backend.Util.NameResolverMultiplexer(context);
		this.add_to_multiplexer(multiplexer, scheme, host, port);
		this.resolver_configuration.foreach_alias(scheme, (alias) => {
			this.add_to_multiplexer(multiplexer, alias.get_scheme(scheme), host, port);
		});
		return multiplexer;
	}
	
	public void add_to_multiplexer(Night.Backend.Util.NameResolverMultiplexer multiplexer, string scheme, string host, string? port){
		this.resolver_configuration.foreach_rule(scheme,(rule) => {
			if (rule.matches_scheme(scheme) && rule.matches_host(host)) {
				Night.Interface.Request.NameCategory? override_category = rule.override_category;
				this.resolver_configuration.foreach_resolver(rule.resolver_group, (resolver) => {
					if (override_category == null) {
						multiplexer.append_resolver(resolver.resolver,resolver.category);
					} else {
						multiplexer.append_resolver(resolver.resolver,override_category);
					}
				});
			}
		});
	}
	
}
