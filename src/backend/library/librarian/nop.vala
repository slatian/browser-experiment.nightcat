public class Night.Interface.Librarian.Nop : Night.Interface.Library.Librarian, Object {
	
	  ///////////////////////////////////////
	 // Night.Interface.Library.Librarian //
	///////////////////////////////////////
	
	public void request(Night.Interface.Library.Context context){
		context.close("error.unkown_verb", context.get_verb().to_string());
	}
	
}
