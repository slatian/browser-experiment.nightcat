public class Night.Backend.Library.LoggedWriter : Night.Interface.ResourceStream.Writer, Object {
	
	private Night.Interface.ResourceStream.Writer writer;
	private Night.Backend.Library.WriterLogger logger;
	private string resource_id;
	private bool closed = false;
	
	public LoggedWriter(Night.Interface.ResourceStream.Writer writer, Night.Backend.Library.WriterLogger logger, string resource_id){
		this.writer = writer;
		this.logger = logger;
		this.resource_id = resource_id;
		writer.awaiting_data.connect(on_awaiting_data);
	}
	
	~LoggedWriter(){
		writer.awaiting_data.disconnect(on_awaiting_data);
	}
	
	private void on_awaiting_data(){
		awaiting_data();
	}
	
	private void on_close(){
		lock(closed){
			if (closed) { return; }
			closed = true;
		}
		logger.log_writer_closed(resource_id);
		writer.awaiting_data.disconnect(on_awaiting_data);
	}
	
	  ///////////////////////////////////////////
	 // Night.Interface.ResourceStream.Writer //
	///////////////////////////////////////////
	
	public bool close(){
		if (writer.close()) {
			on_close();
			return true;
		} else {
			return false;
		}
	}
	
	public bool is_closed(){
		if (writer.is_closed()) {
			if (!closed) {
				on_close();
			}
			return true;
		} else {
			return false;
		}
	}

	public bool set_attribute(string key, string val){
		logger.log_writer_feedback(resource_id, key, val);
		return writer.set_attribute(key, val);
	}
	
	public bool append(uint8[] data, Cancellable? cancellable = null){
		return writer.append(data, cancellable);
	}
	
	public bool can_write(){
		return writer.can_write();
	}
	
}
