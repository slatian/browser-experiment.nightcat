public class Night.Backend.NopWorker.Connector : Night.Interface.Request.Connector, Object {

	public string? request_connection(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.connector.nop", Night.Interface.Request.ContextRole.CONNECTION_SOCKET);
		return "error.invalid_address";
	}
	
	public bool can_handle_address_type(string address_type){
		return false;
	}
	
	public bool can_handle_configuration_key(string address, string key){
		return false;
	}
	
}
