public class Night.Backend.NopWorker.Fetcher : Night.Interface.Request.Fetcher ,Object{
	
	private string error;
	
	public Fetcher(string error = "error.invalid_action"){
		this.error = error;
	}
	
	public bool can_handle_uri(string uri, Night.Interface.Request.Action action){
		return false;
	}
	
	public string? request(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.fetcher.nop", Night.Interface.Request.ContextRole.FETCHER);
		return error;
	}
	
}
