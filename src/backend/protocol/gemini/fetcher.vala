public class Night.Backend.Protocol.Gemini.Fetcher : Night.Interface.Request.Fetcher, Object {
	
	public bool can_handle_uri(string uri, Night.Interface.Request.Action action){
		// do not test for gemini:// to allow for things like gemini+whatever
		return uri.has_prefix("gemini") && action == Night.Interface.Request.Action.DOWNLOAD;
	}
	
	public string? request(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.fetcher.gemini", Night.Interface.Request.ContextRole.FETCHER);
		var config = context.get_my_configuration();
		string? uri = config.get_value("uri");
		string? action = config.get_value("action");
		if (uri == null) { return "error.no_uri"; }
		if (!uri.has_prefix("gemini")) { return "error.invalid_uri"; }
		var request = context.get_request();
		if (action == "download"){
			var parsed_uri = new Night.Util.ParsedUri(uri,false);
			var resolver = context.resolve_authority(parsed_uri.scheme, parsed_uri.authority);
			string? address;
			bool success = false;
			while ((address = resolver.next()) != null) {
				if (context.close_requested()) {
					context.close_because_of_error("error.cancelled");
				}
				string context_uuid = context.call_worker(Fetcher.download_transmission_worker, uri, Night.Interface.Request.Action.DOWNLOAD, null, false, null, address);
				if (request.get_context_successful(context_uuid)){
					context.set_most_significant_child(context_uuid);
					success = true;
					break;
				}
			}
			resolver.close();
			if (success) {
				return null;
			} else {
				return "error.all_transmissions_failed";
			}
		} else {
			return "error.invalid_action";
		}
	}
	
	public static string? download_transmission_worker(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.transmission.gemini", Night.Interface.Request.ContextRole.DOWNLOAD);
		var config = context.get_my_configuration();
		string? uri = config.get_value("uri");
		string? address = config.get_value("address");
		string? action = config.get_value("action");
		if (uri == null) { return "error.no_uri"; }
		if (address == null) { return "error.no_address"; }
		if (action == "download"){
			string context_uuid;
			bool connection_established = context.open_connection(address, download_connection_established_callback, out context_uuid);
			context.set_most_significant_child(context_uuid);
			if (connection_established) {
				return null;
			} else {
				return "error.connection_failed";
			}
		} else {
			return "error.invalid_action";
		}
	}
	
	public static bool download_connection_established_callback(Night.Interface.Request.ContextInterface context, GLib.IOStream connection) throws Error {
		var configuration = context.get_my_configuration();
		string? uri = configuration.get_value("uri");
		string? address = configuration.get_value("address");
		if (uri == null) {
			context.close_because_of_error("error.no_uri");
			return true;
		}
		if (address == null) {
			context.close_because_of_error("error.no_address");
			return true;
		}
		uint64? block_size = configuration.get_number("block_size");
		uint64? size_limit = configuration.get_number("size_limit");
		if (block_size == null) { block_size = 1024*64; }
		if (size_limit == null) { size_limit = 1024*1024*1024*3; }
		
		Night.Interface.ResourceStream.Writer? resource_writer = null;
		Night.Interface.Request.ResourceStreamOutput? resource_output = null;
		
		try {
			connection.output_stream.write(@"$uri\r\n".data);
			var input_stream = new DataInputStream (connection.input_stream);
			
			string? statusline = input_stream.read_line(null);
			if (statusline == null){
				context.close_because_of_error("error.protocol_violaton.gemini.no_statusline");
				return false;
			}
			while(statusline.has_suffix("\r") || statusline.has_suffix("\n")){
				statusline = statusline.substring(0,statusline.length-1);
			}
			if (statusline.length > (1024+3)){
				context.close_because_of_error("error.protocol_violaton.gemini.too_long_statusline");
				return false;
			}
			if (statusline.strip().length < 2){
				context.close_because_of_error("error.protocol_violaton.gemini.too_short_statusline");
				return false;
			}
			var seperator = statusline.substring(2,1);
			if (!(seperator == " " || seperator == "\t")){
				context.close_because_of_error("error.protocol_violaton.gemini.no_seperator");
				return false;
			}
			var statuscode = int.parse(statusline.substring(0,2));
			var metaline = statusline.substring(3);
			context.submit_feedback("gemini.statusline",statusline);
			context.submit_feedback("gemini.statuscode",statuscode.to_string());
			context.submit_feedback("gemini.metaline",metaline);
			switch(statuscode/10){
				case 1:
				case 2:
				case 3:
					resource_output = context.allocate_logged_resource(uri);
					if (resource_output == null) {
						context.close_because_of_error("error.resource_allocation_failed");
						return false;
					}
					resource_writer = resource_output.get_writer();
					resource_writer.set_attribute("uri",uri);
					resource_writer.set_attribute("host_address", address);
					resource_writer.set_attribute("known_incomplete","true");
					break;
				default:
					break;
			}
			switch(statuscode/10){
				case 1: //input
					string file = "";
					if (statuscode == 11) {
						file += @"[password]$(metaline)\n";
					} else {
						file += @"$(metaline)\n";
					}
					file +=  @"$(uri)";
					resource_writer.append(file.data);
					resource_writer.set_attribute("known_incomplete","false");
					resource_writer.set_attribute("mimetype","text/night-input-query");
					resource_writer.close();
					resource_output.mark_as_success();
					return false;
				case 2: //file
					resource_writer.set_attribute("mimetype",metaline);
					uint64 counter = 0;
					while (true){
						if (context.close_requested()) {
							resource_writer.close();
							context.close_because_of_error("error.cancelled");
							return false;
						}
						var bytes = input_stream.read_bytes((size_t) block_size);
						counter += bytes.length;
						if (bytes.length == 0) {
							break;
						} else {
							if (!resource_writer.append(Bytes.unref_to_data(bytes))) {
								resource_writer.close();
								resource_output.mark_as_errored("error.resource_write_failed");
								context.close_because_of_error("error.resource_write_failed");
								return false;
							}
						}
						//terminate early if file gets too big
						if (counter > size_limit) {
							resource_writer.close();
							resource_output.mark_as_errored("error.size_limit_reached");
							context.close_because_of_error("error.size_limit_reached");
							return false;
						}
					}
					resource_writer.set_attribute("known_incomplete","false");
					resource_writer.close();
					resource_output.mark_as_success();
					return false;
				case 3: //redirect
					var joined_uri = Night.Util.Uri.join(uri,metaline);
					if (joined_uri == null){joined_uri = uri;}
					resource_writer.append(joined_uri.data);
					resource_writer.set_attribute("known_incomplete","false");
					resource_writer.set_attribute("mimetype","text/night-redirect");
					switch(statuscode){
						case 30:
							resource_writer.set_attribute("redirect_type","temporary");
							break;
						case 31:
							resource_writer.set_attribute("redirect_type","permanent");
							break;
						default:
							break;
					}
					resource_writer.close();
					resource_output.mark_as_success();
					return false;
				case 4: //temporarely unavaiable					
				case 5: //permanently unavaiable
					context.submit_feedback("server_message",metaline);
					if (statuscode/10 == 4){
						context.submit_feedback("error.is_temporary","true");
					}
					switch(statuscode){
						case 41:
							context.submit_feedback("error.server_unavaiable","true");
							break;
						case 42:
							context.submit_feedback("error.cgi_error","true");
							break;
						case 43:
						case 53:
							context.submit_feedback("error.proxy_error","true");
							break;
						case 44:
							context.submit_feedback("error.rate_limited","true");
							break;
						case 59:
							context.close_because_of_error("error.bad_request");
							return false;
						default:
							break;
					}
					context.close_because_of_error("resource_unavaiable");
					return false;
				case 6: //authentication required
					context.submit_feedback("server_message",metaline);
					switch(statuscode){
						case 60:
							context.submit_feedback("certificate.required","true");
							break;
						case 61:
							context.submit_feedback("certificate.unauthorised","true");
							break;
						case 62:
							context.submit_feedback("certificate.invalid","true");
							break;
						default:
							break;
					}
					context.close_because_of_error("error.authentication_required");
					return false;
				default:
					resource_writer.close();
					context.close_because_of_error("error.protocol_violaton.gemini.invalid_statuscode");
					return false;
			}
			
		} catch (Error e) {
			if (resource_writer != null) {
				resource_writer.close();
				resource_output.mark_as_errored("error.connection_failed");
			}
			context.submit_feedback("error.exception", e.message);
			context.close_because_of_error("error.exception");
			throw e;
		}
	}
	
}
