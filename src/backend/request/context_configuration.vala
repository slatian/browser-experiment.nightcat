public class Night.Backend.Request.ContextConfiguration : Night.Interface.Request.ContextConfiguration, Object {
	
	private HashTable<string,string> attributes = new HashTable<string,string>(str_hash, str_equal);
	private bool writable = true;
	
	public void set_value(string key, string val){
		if (writable) {
			attributes.set(key, val);
		}
	}
	
	public Night.Backend.Request.ContextConfiguration empty_seal(){
		this.writable = false;
		return this;
	}
	
	  ///////////////////////////////////////////////////////
	 //  Night.Interface.Request.ConnectionConfiguration  //
	///////////////////////////////////////////////////////
	
	public string? get_value(string key){
		return attributes.get(key);
	}
	
	public void foreach_key(Func<string> cb){
		attributes.foreach((k,v) => {
			cb(k);
		});
	}
	
	public void seal(){
		writable = false;
	}
	
}
