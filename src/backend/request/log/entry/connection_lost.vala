public class Night.Backend.Request.Log.Entry.ConnectionLost : Night.Interface.Request.Log.Entry, Object {

	private string _context_id;
	private string _reason;

	public ConnectionLost(string context_id, string _reason){
		this._context_id = context_id;
		this._reason = _reason;
	}

	public string get_unlocalized_message(){
		return "_!!!_night.request.log.connection_lost"+"…"+_reason.escape();
	}

	public string get_context_id(){
		return _context_id;
	}

	public string get_event_type(){
		return "connection_lost";
	}

	public void foreach_argument(Func<string> cb){
		cb("reason");
	}

	public string? get_argument(string key){
		switch(key){
			case "reason":
				return _reason;
			default:
				return null;
		}
	}

	public Night.Interface.Request.Log.Severity get_severity(){
		return Night.Interface.Request.Log.Severity.INFO;
	}

	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return null; }
	public string? get_value(){ return null; }
	public string? get_uri(){ return null; }
	public string? get_resource_id(){ return null; }

}
