public class Night.Backend.Request.Log.Entry.DebugMessage : Night.Interface.Request.Log.Entry, Object {

	private string _context_id;
	private string _unlocalized_message;

	public DebugMessage(string context_id, string _unlocalized_message){
		this._context_id = context_id;
		this._unlocalized_message = _unlocalized_message;
	}

	public string get_unlocalized_message(){
		return _unlocalized_message;
	}

	public string get_context_id(){
		return _context_id;
	}

	public string get_event_type(){
		return "debug_message";
	}

	public void foreach_argument(Func<string> cb){
	}

	public string? get_argument(string key){
		switch(key){
			default:
				return null;
		}
	}

	public Night.Interface.Request.Log.Severity get_severity(){
		return Night.Interface.Request.Log.Severity.DEBUG;
	}

	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return null; }
	public string? get_value(){ return null; }
	public string? get_uri(){ return null; }
	public string? get_resource_id(){ return null; }

}
