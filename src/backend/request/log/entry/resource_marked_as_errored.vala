public class Night.Backend.Request.Log.Entry.ResourceMarkedAsErrored : Night.Interface.Request.Log.Entry, Object {

	private string _context_id;
	private string _resource_id;
	private string _most_significant_error;

	public ResourceMarkedAsErrored(string context_id, string _resource_id, string _most_significant_error){
		this._context_id = context_id;
		this._resource_id = _resource_id;
		this._most_significant_error = _most_significant_error;
	}

	public string get_unlocalized_message(){
		return "_!!!_night.request.log.resource_marked_as_errored"+"…"+_resource_id.escape()+"…"+_most_significant_error.escape();
	}

	public string get_context_id(){
		return _context_id;
	}

	public string get_event_type(){
		return "resource_marked_as_errored";
	}

	public void foreach_argument(Func<string> cb){
		cb("resource_id");
		cb("most_significant_error");
	}

	public string? get_argument(string key){
		switch(key){
			case "resource_id":
				return _resource_id;
			case "most_significant_error":
				return _most_significant_error;
			default:
				return null;
		}
	}

	public Night.Interface.Request.Log.Severity get_severity(){
		return Night.Interface.Request.Log.Severity.INFO;
	}

	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return null; }
	public string? get_value(){ return null; }
	public string? get_uri(){ return null; }
	public string? get_resource_id(){ return _resource_id; }

}
