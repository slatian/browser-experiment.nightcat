public class Night.Backend.Request.Request : Night.Interface.Request.Request, Night.Interface.Request.ResourceAllocator, Object {
	
	private Cancellable cancellable = new Cancellable();
	private string uuid = GLib.Uuid.string_random();
	private Night.Interface.Request.Action _action;
	private string _uri;
	private Night.Interface.Request.RequestContext? request_context = null;
	private Night.Interface.ResourceStream.Receiver? resource = null;
	private Night.Backend.Request.Log.Logger logger = new Night.Backend.Request.Log.Logger();
	private HashTable<string,Context> contexts = new HashTable<string,Context>(str_hash, str_equal);
	private HashTable<string,ResourceStatus> resources = new HashTable<string,ResourceStatus>(str_hash, str_equal);
	private string _root_context_uuid = "";
	private string? cancel_reason = null;
	
	private uint32 number_of_open_contexts = 0;
	
	public Request(string uri, Night.Interface.Request.Action action, Night.Interface.Request.RequestContext request_context, Night.Interface.ResourceStream.Receiver? resource = null){
		this._uri = uri;
		this._action = action;
		this.request_context = request_context;
		this.resource = resource;
		this.request_context.set_request_object(this);
	}
	
	private void append_log_entry(Night.Interface.Request.Log.Entry entry){
		logger.append_log_entry(entry);
		if (request_context != null) {
			request_context.append_log_entry(entry);
		}
	}
	
	private void context_closed_hook(){
		number_of_open_contexts--;
		if (number_of_open_contexts == 0){
			lock(request_context){
				//Avoid keeping an unused reference, that could lead to a memory leak
				this.request_context = null;
			}
		}
	}
	
	 /////////////////////////////////////////////
	// Feedback interface for context interfaces
	
	public void open_context(string context_uuid, string name, string? parent_id, Night.Interface.Request.ContextRole role, Night.Interface.Request.ContextConfiguration configuration){
		lock(logger) {
			if (parent_id == null){
				this._root_context_uuid = context_uuid;
			}
			this.contexts.set(context_uuid, new Context(name, context_uuid, configuration, role, parent_id));
			this.append_log_entry(new Night.Backend.Request.Log.Entry.ContextCreated(context_uuid, parent_id, role));
			number_of_open_contexts++;
		}
	}
	
	public void submit_feedback(string context_uuid, string key, string val){
		lock(logger) {
			var context = contexts.get(context_uuid);
			if (context == null){ return; }
			context.feedback.set(key, val);
			this.append_log_entry(new Night.Backend.Request.Log.Entry.ContextAttributeSet(context_uuid, key, val));
		}
	}
	
	public Night.Interface.ResourceStream.Writer? submit_logged_resource_stream(string context_uuid, Night.Interface.ResourceStream.Writer writer, string uri, out string resource_id){
		resource_id = "";
		lock(logger) {
			var context = contexts.get(context_uuid);
			if (context == null){ return null; }
			string _resource_id = "";
			do {
				_resource_id = GLib.Uuid.string_random();
			} while (resources.get(_resource_id) != null);
			resource_id = _resource_id;
			resources.set(resource_id, new ResourceStatus(context_uuid, uri));
			this.append_log_entry(new Night.Backend.Request.Log.Entry.ResourceSubmitted(context_uuid, _resource_id, uri));
			var logged_writer = new Night.Backend.Request.Helper.LoggedStreamWriter(writer, context_uuid, _resource_id);
			logged_writer.log_event.connect(this.append_log_entry);
			return logged_writer;
		}
	}
	
	public void mark_resource_as_success(string context_uuid, string resource_id){
		lock(logger) {
			var resource_status = resources.get(resource_id);
			if (resource_status == null){ return; }
			if (resource_status.context_uuid != context_uuid){ return; }
			if (resource_status.success || resource_status.most_significant_error != null){ return; }
			resource_status.success = true;
			number_of_open_contexts++;
			this.append_log_entry(new Night.Backend.Request.Log.Entry.ResourceMarkedAsSuccess(context_uuid, resource_id));
		}
	}
	
	public void mark_resource_as_errored(string context_uuid, string resource_id, string most_significant_error){
		lock(logger) {
			var resource_status = resources.get(resource_id);
			if (resource_status == null){ return; }
			if (resource_status.context_uuid != context_uuid){ return; }
			if (resource_status.success || resource_status.most_significant_error != null){ return; }
			resource_status.most_significant_error = most_significant_error;
			this.append_log_entry(new Night.Backend.Request.Log.Entry.ResourceMarkedAsErrored(context_uuid, resource_id, most_significant_error));
		}
	}
	
	public void set_most_significant_child(string context_uuid, string? most_significant_child){
		lock(logger) {
			var context = contexts.get(context_uuid);
			if (context == null){ return; }
			if (most_significant_child != null){
				var child_context = contexts.get(most_significant_child);
				if (child_context == null){ return;}
				if (child_context.parent_id != context_uuid) { return; }
			}
			context.most_significant_child = most_significant_child;
			this.append_log_entry(new Night.Backend.Request.Log.Entry.ContextMostSignificantChildSet(context_uuid, most_significant_child));
		}
	}
	
	public void close_context_because_of_success(string context_uuid){
		lock(logger) {
			var context = contexts.get(context_uuid);
			if (context == null){ return; }
			if (context.success || context.most_significant_error != null){ return; }
			context.success = true;
			context_closed_hook();
			this.append_log_entry(new Night.Backend.Request.Log.Entry.ContextClosed(context_uuid, null));
		}
	}
	
	public void close_conetxt_because_of_error(string context_uuid, string most_significant_error){
		lock(logger) {
			var context = contexts.get(context_uuid);
			if (context == null){ return; }
			if (context.success || context.most_significant_error != null){ return; }
			context.most_significant_error = most_significant_error;
			if (context.feedback.get(most_significant_error) == null){
				context.feedback.set(most_significant_error, "true");
			}
			context_closed_hook();
			this.append_log_entry(new Night.Backend.Request.Log.Entry.ContextClosed(context_uuid, most_significant_error));
		}
	}
	
	/*
	public void log_subrequest(string context_uuid, Night.Interface.Request.Request subrequest){
		lock(logger) {
			var resource = subrequest.get_attached_resource();
			string? resource_id = null;
			if (resource != null) {
				//TODO: figure out a way to make this less hacky
				resource_id = "";
			}
			this.append_log_entry(new Night.Backend.Request.Log.Entry.SubrequestMade(context_uuid, subrequest.get_request_uuid(), subrequest.get_uri(), subrequest.get_action().to_string(), resource_id));
		}
	}
	*/
	
	  /////////////////////////////////////////////////
	 //  Night.Interface.Request.ResourceAllocator  //
	/////////////////////////////////////////////////
	
	public Night.Interface.ResourceStream.Writer? allocate_resource_for_request(Night.Interface.Request.Request request, string context_uuid, string uri){
		lock(request_context){
			if (request_context == null || request != this){
				return null;
			}
			var context = contexts.get(context_uuid);
			if (context == null){ return null; }
			return request_context.request_resource_download_destination(context_uuid, uri);
		}
	}
	
	  ///////////////////////////////////////
	 //  Night.Interface.Request.Request  //
	///////////////////////////////////////
	
	public Cancellable get_cancellable_object(){
		return cancellable;
	}
	
	public void cancel(string reason){
		lock(logger) {
			this.cancel_reason = reason;
			this.cancellable.cancel();
			this.append_log_entry(new Night.Backend.Request.Log.Entry.CancelRequested(_root_context_uuid, reason));
		}
	}
	
	public string? get_cancellation_reason(){
		return cancel_reason;
	}
	
	public bool was_cancelled(){
		return cancellable.is_cancelled();
	}
	
	public string get_request_uuid(){
		return uuid;
	}
	
	//the data it was submitted with	
	public Night.Interface.Request.Action get_action(){
		return _action;
	}
	
	public string get_uri(){
		return _uri;
	}
	
	public Night.Interface.ResourceStream.Receiver? get_attached_resource(){
		return resource;
	}

	// retrieve context information
	public string get_root_context_uuid(){
		return _root_context_uuid;
	}
	
	public Night.Interface.Request.ContextRole? get_context_role(string context_uuid){
		var context = contexts.get(context_uuid);
		if (context == null){ return null; }
		return context.role;
	}
	
	public string? get_context_name(string context_uuid){
		var context = contexts.get(context_uuid);
		if (context == null){ return null; }
		return context.name;
	}
	
	public string? get_context_uri(string context_uuid){
		var context = contexts.get(context_uuid);
		if (context == null){ return null; }
		return context.get_uri();
	}
	
	public string? get_parent_context_id(string context_uuid){
		var context = contexts.get(context_uuid);
		if (context == null){ return null; }
		return context.parent_id;
	}
	
	public void foreach_direct_child_context_id(Func<string> cb, string context_uuid){
		contexts.foreach((k,c) => {
			if (c.parent_id == context_uuid){
				cb(k);
			}
		});
	}
	
	public Night.Interface.Request.ContextConfiguration? get_context_configuration(string context_uuid){
		var context = contexts.get(context_uuid);
		if (context == null){ return null; }
		return context.configuration;
	}
	
	public string? get_context_feedback_value(string context_uuid, string key){
		var context = contexts.get(context_uuid);
		if (context == null){ return null; }
		return context.feedback.get(key);
	}
	
	public void foreach_context_feedback(HFunc<string,string> cb, string context_uuid){
		var context = contexts.get(context_uuid);
		if (context == null){ return; }
		context.feedback.foreach((k,v) => {
			cb(k,v);
		});
	}
	
	public string? get_context_most_significant_child_id(string context_uuid){
		var context = contexts.get(context_uuid);
		if (context == null){ return null; }
		return context.most_significant_child;
	}
	
	public string? get_context_most_significant_error(string context_uuid){
		var context = contexts.get(context_uuid);
		if (context == null){ return null; }
		return context.most_significant_error;
	}
	
	public bool get_context_successful(string context_uuid){
		var context = contexts.get(context_uuid);
		if (context == null){ return false; }
		return context.success;
	}
	
	public void foreach_submitted_resurce_id(Func<string> cb, string? context_uuid, string? uri){
		resources.foreach((id,sr) => {
			if (sr.context_uuid == context_uuid || context_uuid == null){
				if (sr.uri == uri || uri == null){
					cb(id);
				}
			}
		});
	}
	
	public bool is_context_open(string context_uuid){
		var context = contexts.get(context_uuid);
		if (context == null){ return false; }
		return context.is_open();
	}
	
	public bool is_any_context_open() {
		return number_of_open_contexts != 0;
	}
	
	public Night.Interface.Request.Log.Log get_log(){
		return logger;
	}
	
	//retrieve the uri the request was submitted with somewhere down the most successful child path
	
	public string? get_resource_uri(string resource_id) {
		var resource_status = resources.get(resource_id);
		if (resource_status == null) { return null; }
		return resource_status.uri;
	}
	
	public string? get_resource_context_id(string resource_id){
		var resource_status = resources.get(resource_id);
		if (resource_status == null) { return null; }
		return resource_status.context_uuid;
	}
	
	public string? get_resource_most_significant_error(string resource_id){
		var resource_status = resources.get(resource_id);
		if (resource_status == null) { return null; }
		return resource_status.most_significant_error;
	}
	
	public bool get_resource_successful(string resource_id){
		var resource_status = resources.get(resource_id);
		if (resource_status == null) { return false; }
		return resource_status.success;
	}
}

private class Night.Backend.Request.Context : Object {
	public string name;
	public string uuid;
	public string? parent_id;
	public Night.Interface.Request.ContextConfiguration configuration;
	public Night.Interface.Request.ContextRole role;
	public HashTable<string,string> feedback = new HashTable<string,string>(str_hash, str_equal);
	public string? most_significant_child = null;
	public string? most_significant_error = null;
	public bool success = false;
	
	public Context(string name, string uuid, Night.Interface.Request.ContextConfiguration configuration, Night.Interface.Request.ContextRole role, string? parent_id = null){
		this.name = name;
		this.uuid = uuid;
		this.configuration = configuration;
		this.role = role;
		this.parent_id = parent_id;
	}
	
	public bool is_open(){
		return (!success) && (most_significant_error == null);
	}
	
	public string? get_uri(){
		return configuration.get_value("uri");
	}
	
}

private class Night.Backend.Request.ResourceStatus : Object {
	public string context_uuid;
	public string uri;
	public string? most_significant_error = null;
	public bool success = false;
	
	public ResourceStatus(string context_uuid, string uri){
		this.context_uuid = context_uuid;
		this.uri = uri;
	}
}
