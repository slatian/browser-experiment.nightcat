public class Night.Backend.Resolver.TlsHostResolver : Night.Interface.Request.NameResolver, Object {
	
	public Night.Interface.Request.NameResolverResult lookup(Night.Interface.Request.ContextInterface context){
		return new Nigh.TestSession.TlsHostResolverResult(context);
	}	
	
}

public class Nigh.TestSession.TlsHostResolverResult : Night.Interface.Request.NameResolverResult, Object {
	
	private bool closed = false;
	private Night.Interface.Request.ContextInterface context;
	private Night.Interface.Request.NameResolverResult? result;
	private string? host;
	private string? port;
	
	public TlsHostResolverResult(Night.Interface.Request.ContextInterface context){
		this.context = context;
		context.initalize_interface("night.resolver.tls_host_resolver", Night.Interface.Request.ContextRole.HOST_RESOLVER);
		var configuration = context.get_my_configuration();
		host = configuration.get_value("hostname");
		port = configuration.get_value("port");
		string? scheme = configuration.get_value("scheme");
		if (scheme == null) {
			scheme = "";
		} else if (scheme == "tls") {
			scheme = "";
		} else if (scheme.has_prefix("tls+")) {
			var split = scheme.split("+",2);
			scheme = split[1];
		}
		if (host == null) {
			context.close_because_of_error("error.no_host_specified");
			return;
		}
		if (port == null) {
			context.close_because_of_error("error.no_port_specified");
			return;
		}
		result = context.resolve_host(scheme,host,port);
	}
	
	//if the next function returns null this means there are no entrys left and the resolver will close automatically, the next function will block for the time the lookup takes
	public string? next(out Night.Interface.Request.NameCategory category = null, out int rating = null){
		category = Night.Interface.Request.NameCategory.NEUTRAL;
		rating = 0;
		if (!context.is_open()) { return null; }
		if (context.close_requested()) {
			context.close_because_of_error("cancelled");
			return null;
		}
		if (closed) { return null; }
		if (result == null) { return null; }
		string? address = result.next(out category, out rating);
		if (address == null) {
			close();
			return null;
		}
		address = @"!tls,$(host.escape()),$(port.escape())$address";
		context.submit_feedback("resolver.current_address",address);
		return address;
	}
	
	public void close(){
		closed = true;
		if (result != null){
			result.close();
		}
		context.close_because_of_success();
	}
	
	public bool is_closed(){
		return closed;
	}
	
	public Night.Interface.Request.NameCategory? peek_next_category(){
		if (this.result == null) { return null; }
		return this.result.peek_next_category();
	}
	
}
