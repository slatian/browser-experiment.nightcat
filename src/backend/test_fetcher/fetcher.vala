public class Night.Backend.TestFetcher.Fetcher : Night.Interface.Request.Fetcher ,Object{
	
	public bool can_handle_uri(string uri, Night.Interface.Request.Action action){
		return uri.has_prefix("test://") && action == Night.Interface.Request.Action.DOWNLOAD;
	}
	
	public string? request(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.fetcher.test", Night.Interface.Request.ContextRole.FETCHER);
		var config = context.get_my_configuration();
		string? uri = config.get_value("uri");
		string? action = config.get_value("action");
		if (uri == null) { return "error.no_uri"; }
		if (!uri.has_prefix("test://")) { return "error.invalid_uri"; }
		if (action == "download"){
			var resource_output = context.allocate_logged_resource(uri);
			if (resource_output == null) { return "error.resource_allocation_failed"; }
			var resource_writer = resource_output.get_writer();
			//TODO: catch errors while writing to the resorce
			resource_writer.append("This is a test resorce\n".data);
			resource_writer.append(uri.data);
			resource_writer.close();
			resource_output.mark_as_success();
			return null;
		} else {
			return "error.invalid_action";
		}
	}
	
}
