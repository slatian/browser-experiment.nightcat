public class Night.Backend.Util.SocketAddress : Object {
	
	public string address_type { get; protected set; }
	public string arglist { get; protected set; }
	public Night.Backend.Util.SocketAddress? sub_address { get; protected set; default = null; }
	
	protected SocketAddress.populated(string address_type, string arglist){
		this.address_type = address_type;
		this.arglist = arglist;
	}
	
	public SocketAddress(string address_type){
		this.address_type = GLib.Uri.escape_string(address_type,"+");
		this.arglist = "";
	}
	
	public string to_string(){
		if (sub_address != null) {
			return @"!$address_type,$arglist$(sub_address.to_string())";
		} else {
			return @"!$address_type,$arglist";
		}
	}
	
	public void append_argument(string val){
		if(arglist != "") {
			arglist = arglist+",";
		}
		arglist = arglist+GLib.Uri.escape_string(val);
	}
	
	public string[] get_arguments(){
		string[] arguments = arglist.split(",");
		for (int i = 0; i<arguments.length; i++){
			arguments[i] = GLib.Uri.unescape_string(arguments[i]);
		}
		return arguments;
	}
	
	public static SocketAddress? from_string(string address){
		if (address.has_prefix("!")) {
			string[] addresses = address.split("!",3);
			if (addresses.length < 2) { return null; }
			string[] address_split = addresses[1].split(",",2);
			if (address_split.length != 2) { return null; }
			var address_obj = new SocketAddress.populated(address_split[0],address_split[1]);
			if (addresses.length == 3) {
				address_obj.sub_address = SocketAddress.from_string("!"+addresses[2]);
			}
			return address_obj;
		}
		return null;
	}
	
}
