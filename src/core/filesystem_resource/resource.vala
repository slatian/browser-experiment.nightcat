public class Night.FilesystemResource.Resource : Night.Interface.Resource.Resource, Object {

	public string filepath { get; protected set; }
	public File file { get; protected set; }
	public Night.Interface.Resource.Mode resource_mode { get; protected set; default = Night.Interface.Resource.Mode.STATIC; }
	public bool committing { get; protected set; default = false;}
	public bool appending { get; protected set; default = false;}
	
	public Night.Interface.Log.Logger? logger {
		get {
			return log.logger;
		}
		set {
			log.set_backend_logger(value);
		}
	}
	
	public signal void file_updated(bool append_only);
	public signal void file_deleted();
	
	private bool _exists = false;
	private HashTable<string,string> atteributes = new HashTable<string,string>(str_hash,str_equal);
	private Night.Util.Eventbus? resource_eventbus = null;
	private Night.ModuleHelper.Log.Loginput log;
	private string uid;
	
	public Resource(string filepath, bool writable = true, Night.Interface.Log.Logger? logger = null, string uid){
		this.uid = uid;
		log = new Night.ModuleHelper.Log.Loginput(filepath);
		log.set_backend_logger(logger);
		this.filepath = filepath;
		if (writable) { this.resource_mode = Night.Interface.Resource.Mode.UPDATABLE; }
		this.file = File.new_for_path(filepath);
		try {
			this.file.create(GLib.FileCreateFlags.PRIVATE).close();
			this._exists = true;
		}	catch (Error e){
			//nothing has to be done, _exists is false by default
		}
	}
	
	public bool delete(){
		if (file.query_exists()){
			try{
				file.delete();
				this.set_mode(Night.Interface.Resource.Mode.DELETED);
				this.file_deleted();
				return true;
			}catch( Error e ){
				return false;
			}
		}
		return true; //is already deleted if file doesn't exist anymore
	}
	
	 ////////////////////////
	// Resource Writer API
	
	public bool set_attribute(string key, string val){
		atteributes.set(key,val);
		return true;
	}
	
	public bool set_mode(Night.Interface.Resource.Mode mode){
		lock(resource_mode){
			if (this.resource_mode == Night.Interface.Resource.Mode.DELETED) { return false; }
			if (resource_mode < mode) { return false; }
			this.resource_mode = mode;
			this.mode_updated(this);
			return true;
		}
	}
	
	public void set_eventbus(Night.Util.Eventbus? eventbus){
		this.resource_eventbus = eventbus;
		this.eventbus_set(this);
	}
	
	public bool can_commit(bool append_only){
		lock(resource_mode){
			if (resource_mode == Night.Interface.Resource.Mode.UPDATABLE) { return true; }
			if (resource_mode == Night.Interface.Resource.Mode.APPENDABLE && append_only){ return true; }
			return false;
		}
	}
	
	public bool start_commit(bool append_only, out bool? please_wait = null){
		please_wait = false;
		if (this.resource_mode == Night.Interface.Resource.Mode.DELETED) { return false; }
		lock(committing){
			log.debug(@"commit start requested append_only:$append_only");
			if (committing) {
				log.debug("There is already another commit ongoing, denyig commit");
				please_wait = true;
				return false;
			}
			if (can_commit(append_only)) {
				log.debug("No other commit ongoing and can_commit returned true, starting commit");
				committing = true;
				appending = append_only;
				return true;
			}
		}
		log.debug(@"Denying commit, modes do not match! (mode:$resource_mode)");
		return false;
	}
	
	public void end_commit(){
		lock(committing){
			committing = false;
			this.file_updated(appending);
			this.size_updated(this);
		}
	}
	
	public Night.FilesystemResource.Writer get_writer(){
		return new Writer(this,log);
	}
	
	  ///////////////////////////////////////
	 // Night.Interface.Resource.Resource //
	///////////////////////////////////////
	
	public string get_resource_uid(){
		return uid;
	}
	
	public Night.Interface.Resource.Mode get_mode(){
		return this.resource_mode;
	}

	public string? get_attribute(string key){
		return atteributes.get(key);
	}
	
	public uint64 get_current_size(){
		try {
			var fileinfo = this.file.query_info("standard::size",GLib.FileQueryInfoFlags.NONE);
			return fileinfo.get_size();
		} catch (Error e) {
			return 0;
		}
	}
	
	public bool exists(){
		return _exists;
	}
	
	public Night.Util.Eventbus? get_eventbus(){
		return resource_eventbus;
	}
	
	public string? get_local_url(){
		return "file://"+filepath; //Naive approach
	}
	
	public Night.Interface.Resource.Reader get_reader(){
		return new Reader(this);
	}
}
