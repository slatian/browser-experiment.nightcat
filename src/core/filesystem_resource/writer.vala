public class Night.FilesystemResource.Writer : Night.Interface.Resource.Writer, Object {
	
	public string filepath { get; protected set; }
	public File file { get; protected set; }
	public WriterMode mode { get; protected set; default = WriterMode.NONE; }
	
	private Night.FilesystemResource.Resource _resource;
	private FileOutputStream? output_stream = null;
	private bool closed = false;
	private Night.ModuleHelper.Log.Loginput log;
	
	public Writer(Night.FilesystemResource.Resource resource, Night.Interface.Log.Logger? logger = null){
		string uuid = GLib.Uuid.string_random();
		log = new Night.ModuleHelper.Log.Loginput(@"writer-$uuid");
		log.set_backend_logger(logger);
		this._resource = resource;
		this.filepath = @"$(_resource.filepath)_$uuid.commit.tmp";
		this.file = File.new_for_path(this.filepath);
	}
	
	~Writer(){
		this.close();
	}
	
	//designed to be wrapped inside a lock(output_stream)
	private bool open_output_stream(){
		if (closed) { return false; }
		if (output_stream == null) {
			try{
				output_stream = this.file.create(FileCreateFlags.PRIVATE | FileCreateFlags.REPLACE_DESTINATION);
				return true;
			} catch (Error e) {
				return false;
			}
		} else {
			return true;
		}
	}
	
	private void append_mode(){
		if (mode == WriterMode.NONE){
			this.mode = WriterMode.APPEND;
		}
	}
	
	  /////////////////////////////////////
	 // Night.Interface.Resource.Writer //
	/////////////////////////////////////
	
	public bool clear(Cancellable? cancellable = null){
		if (closed) { return false; }
		reset();
		mode = WriterMode.REPLACE;
		return true;
	}
	
	public bool append(uint8[] data, Cancellable? cancellable = null){
		if (closed) { return false; }
		lock(output_stream){
			if (open_output_stream()) {
				var old_mode = mode;
				append_mode();
				try {
					output_stream.write(data);
					return true;
				} catch (Error e) {
					mode = old_mode;
					return false;
				}
			} else {
				return false;
			}
		}
	}
	
	public bool commit(Cancellable? cancellable = null){
		if (closed) { return false; }
		switch (mode) {
			case WriterMode.REPLACE:
				return replace_commit();
			case WriterMode.APPEND:
				return append_commit();
			case WriterMode.NONE:
			default:
				return false;
		}
	}
	
	private bool append_commit(Cancellable? cancellable = null){
		lock(output_stream){
			if (mode != WriterMode.APPEND) {
				log.debug(@"Not starting append commit because writer mode is $mode");
				return false;
			}
			log.debug("Starting append commit");
			if (_resource.start_commit(true)){
				try {
					var input_stream = file.read();
					var output_stream = _resource.file.append_to(GLib.FileCreateFlags.PRIVATE, cancellable);
					uint8[] buffer = new uint8[65536];
					while (true) {
						var size = input_stream.read(buffer);
						log.debug(@"appending $size bytes of data");
						if (size == 0) { break; }
						if (size == buffer.length) {
							output_stream.write(buffer);
						} else {
							output_stream.write(buffer[0:size]);
						}
					}
					output_stream.flush();
					output_stream.close();
					_resource.end_commit();
					reset();
					return true;
				} catch (Error e) {
					log.error("Error during append commit:\n"+e.message);
					_resource.end_commit();
					return false;
				}
			} else {
				log.debug(@"Couldn't start append commit because the resource didn't give permission to do so! resource mode is '$(_resource.resource_mode)'");
				return false;
			}
		}
	}
	
	public bool delete_resource(){
		return _resource.delete();
	}
	
	private bool replace_commit(){
		lock(output_stream){
			if (mode != WriterMode.REPLACE) { return false; }
			if (_resource.start_commit(false)){
				try {
					file.move(_resource.file, FileCopyFlags.OVERWRITE);
					_resource.end_commit();
					reset();
					return true;
				} catch (Error e) {
					_resource.end_commit();
					return false;
				}
			} else {
				return false;
			}
		}
	}
	
	public void reset(){
		lock(output_stream){
			mode = WriterMode.NONE;
			if (output_stream != null){
				try{
					output_stream.close();
				}catch( Error e ){
					//do nothing
				}
				output_stream = null;
			}
			if (file.query_exists()){
				try{
					file.delete();
				}catch( Error e ){
					//do nothing
				}
			}
		}
	}
	
	//convenience function, may also be used as an optimization if no other clears and appends are staged
	public bool append_and_commit(uint8[] data, Cancellable? cancellable = null){
		if (closed) { return false; }
		if (mode == WriterMode.NONE) {
			if (_resource.start_commit(true)){
				try {
					var output_stream = _resource.file.append_to(GLib.FileCreateFlags.PRIVATE, cancellable);
					output_stream.write(data,cancellable);
					output_stream.close();
					_resource.end_commit();
					return true;
				} catch (Error e) {
					_resource.end_commit();
					return false;
				}
			} else {
				return false;
			}
		} else {
			if (!append(data, cancellable)) { return false; }
			if (!commit(cancellable)) {
				reset();
				return false;		
			}
			return true;
		}
	}
	
	public bool writable(){
		return _resource.resource_mode.writable();
	}
	
	public bool updatable(){
		return _resource.resource_mode == Night.Interface.Resource.Mode.UPDATABLE;
	}
	
	public bool can_commit(){
		if (mode == WriterMode.NONE){
			return false;
		}
		return _resource.can_commit(mode == WriterMode.APPEND);
	}
	
	public bool is_closed(){
		return closed;
	}
	
	//the following functions are applyed immedeantly and do not require a commit
	public bool close(){
		lock(closed){
			if (closed) { return false; }
			this.closed = true;
			reset();
			return true;
		}
	}
	
	public bool downgrade_append_only(){
		if (closed) { return false; }
		return _resource.set_mode(Night.Interface.Resource.Mode.APPENDABLE);
	}
	
	public bool downgrade_read_only(){
		if (closed) { return false; }
		return _resource.set_mode(Night.Interface.Resource.Mode.STATIC);
	}
	
	public bool set_eventbus(Night.Util.Eventbus eventbus){
		if (closed) { return false; }
		_resource.set_eventbus(eventbus);
		return true;
	}
	
	public bool set_attribute(string key, string val){
		if (closed) { return false; }
		return _resource.set_attribute(key,val);
	}
	
	public Night.Interface.Resource.Resource get_resource(){
		return this._resource;
	}
}

public enum Night.FilesystemResource.WriterMode {
	NONE,
	REPLACE,
	APPEND;
	
	public string to_string(){
		switch(this){
			case NONE:
				return "none";
			case REPLACE:
				return "replace";
			case APPEND:
				return "append";
			default:
				return "unknown";
		}
	}
}
