public class Night.ModuleHelper.Log.Loginput : Night.Interface.Log.Logger, Object {
	
	public bool ignore_error = false;
	public bool ignore_warning = false;
	public bool ignore_info = false;
	public bool ignore_debug = false;
	
	public Night.Interface.Log.Logger? logger { get; protected set; default = null; }
	public bool print_log = false;
	public string name;
	
	public Loginput(string name){
		this.name = name;
	}
	
	public void set_backend_logger(Night.Interface.Log.Logger? logger){
		lock(this.logger){
			this.logger = logger;
		}
	}
	
	public void error(string message){
		log_direct(this.name,message,Night.Interface.Log.Severity.ERROR);
	}
	
	public void warning(string message){
		log_direct(this.name,message,Night.Interface.Log.Severity.WARNING);
	}
	
	public void info(string message){
		log_direct(this.name,message,Night.Interface.Log.Severity.INFO);
	}
	
	public void debug(string message){
		log_direct(this.name,message,Night.Interface.Log.Severity.DEBUG);
	}
	
	public void log(string name, string message, Night.Interface.Log.Severity severity){
		log_direct(this.name+">"+name, message, severity);
	}
	
	private void log_direct(string name, string message, Night.Interface.Log.Severity severity){
		if (severity == Night.Interface.Log.Severity.ERROR && ignore_error){ return; }
		if (severity == Night.Interface.Log.Severity.WARNING && ignore_warning){ return; }
		if (severity == Night.Interface.Log.Severity.INFO && ignore_info){ return; }
		if (severity == Night.Interface.Log.Severity.DEBUG && ignore_debug){ return; }
		if (print_log) {
			string time = new GLib.DateTime.now().format("%Y-%m-%d %H:%M:%S");
			print(@"[$time][$name][$severity] $message\n");
		}
		lock(logger){
			if (logger != null){
				logger.log(name,message,severity);
			}
		}
	}
}
