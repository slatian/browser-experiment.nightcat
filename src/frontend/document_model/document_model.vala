public class Night.Frontend.DocumentModel.DocumentModel : Night.Interface.DocumentModel.DocumentModel, Object {
	
	private HashTable<string,Night.Frontend.DocumentModel.DocumentNode> nodes = new HashTable<string,Night.Frontend.DocumentModel.DocumentNode>(str_hash,str_equal);
	
	private string root_node_uuid;
	
	public DocumentModel() {
		var dnode = new DocumentNode(Night.Interface.DocumentModel.NodeType.RESOURCE);
		nodes.set(dnode.uuid, dnode);
		root_node_uuid = dnode.uuid;
	}
	
	private void on_node_created(DocumentNode dnode){
		string? parent = null;
		if (dnode.parent != null){
			parent = dnode.parent.uuid;
		}
		this.node_created(dnode.uuid,parent);
	}
	
	public string to_string(){
		var dnode = nodes.get(root_node_uuid);
		if (dnode == null) { return "NO ROOT NODE FOUND"; }
		return render(dnode);
	}
	
	private string render(DocumentNode dnode, string prefix = "", string? entry_prefix = null){
		string res;
		if (entry_prefix != null) {
			res = @"$entry_prefix$dnode\n";
		} else {
			res = @"$prefix$dnode\n";
		}
		DocumentNode child = dnode.first_child;
		bool first = true;
		while (child != null) {
			string pfx = prefix;
			if (dnode.next != null) {
				pfx += "|";
			} else {
				pfx += " ";
			}
			string epfx;
			if (first) {
				epfx = pfx+"`";
				first = false;
			} else {
				epfx = pfx+" ";
			}
			res += render(child,pfx+" ",epfx);
			child = child.next;
		}
		return res;
	}
	
	  /////////////////////////////////////////////////
	 // Night.Interface.DocumentModel.DocumentModel //
	/////////////////////////////////////////////////
	
	public bool can_write(){
		return true;
	}
	
	public string get_root_node(){
		lock (nodes) {
			return root_node_uuid;
		}
	}
	
	public bool is_node(string node){
		lock (nodes) {
			return nodes.contains(node);
		}
	}
	
	public string? get_parent_node(string node){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			if (dnode.parent == null) { return null; }
			return dnode.parent.uuid;
		}
	}
	
	public string? get_first_child_node(string node){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			if (dnode.first_child == null) { return null; }
			return dnode.first_child.uuid;
		}
	}
	
	public string? get_last_child_node(string node){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			if (dnode.last_child == null) { return null; }
			return dnode.last_child.uuid;
		}
	}
	
	public string? get_next_node(string node){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			if (dnode.next == null) { return null; }
			return dnode.next.uuid;
		}
	}
	
	public string? get_previous_node(string node){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			if (dnode.previous == null) { return null; }
			return dnode.previous.uuid;
		}
	}
	
	public string? get_node_text(string node){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			return dnode.text;
		}
	}
	
	public Night.Interface.DocumentModel.NodeType? get_node_type(string node){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			return dnode.node_type;
		}
	}
	
	public string[]? get_flags(string node){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			return dnode.flags.list_flags();
		}
	}
	
	public bool has_flag(string node, string flag){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return false; }
			return dnode.flags.has_flag(flag);
		}
	}
	
	
	public string? append_child_node(string node, Night.Interface.DocumentModel.NodeType type, string text){
		DocumentNode new_node;
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			new_node = new DocumentNode(type,text);
			new_node.parent = dnode;
			if (dnode.last_child != null) {
				new_node.previous = dnode.last_child;
				dnode.last_child.next = new_node;
				dnode.last_child = new_node;
			} else {
				dnode.last_child = new_node;
				dnode.first_child = new_node;
			}
			nodes.set(new_node.uuid,new_node);
			this.on_node_created(new_node);
			return new_node.uuid;
		}
	}
	
	public string? prepend_child_node(string node, Night.Interface.DocumentModel.NodeType type, string text){
		DocumentNode new_node;
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			new_node = new DocumentNode(type,text);
			new_node.parent = dnode;
			if (dnode.first_child != null) {
				new_node.next = dnode.first_child;
				dnode.first_child.previous = new_node;
				dnode.first_child = new_node;
			} else {
				dnode.last_child = new_node;
				dnode.first_child = new_node;
			}
			nodes.set(new_node.uuid,new_node);
			this.on_node_created(new_node);
			return new_node.uuid;
		}
	}
	
	public string? insert_node_after(string node, Night.Interface.DocumentModel.NodeType type, string text){
		DocumentNode new_node;
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			new_node = new DocumentNode(type,text);
			new_node.parent = dnode.parent;
			new_node.next = dnode.next;
			new_node.previous = dnode;
			dnode.next = new_node;
			if (new_node.next != null){
				new_node.next.previous = new_node;
			}
			if (new_node.parent != null){
				if (new_node.parent.last_child == dnode){
					new_node.parent.last_child = new_node;
				}
			}
			nodes.set(new_node.uuid,new_node);
			this.on_node_created(new_node);
			return new_node.uuid;
		}
	}
	
	public string? insert_node_before(string node, Night.Interface.DocumentModel.NodeType type, string text){
		DocumentNode new_node;
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return null; }
			new_node = new DocumentNode(type,text);
			new_node.parent = dnode.parent;
			new_node.previous = dnode.previous;
			new_node.next = dnode;
			dnode.previous = new_node;
			if (new_node.previous != null){
				new_node.previous.next = new_node;
			}
			if (new_node.parent != null){
				if (new_node.parent.first_child == dnode){
					new_node.parent.first_child = new_node;
				}
			}
			nodes.set(new_node.uuid,new_node);
			this.on_node_created(new_node);
			return new_node.uuid;
		}
	}
	
	public bool replace_node(string node, Night.Interface.DocumentModel.NodeType type, string text){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return false; }
			dnode.node_type = type;
			dnode.text = text;
			this.node_modified(node);
			return true;
		}
	}
	
	public bool remove_node(string node){
		lock (nodes) {
			if (node == root_node_uuid) {
				return false;
			}
			var dnode = nodes.get(node);
			if (dnode == null) { return false; }
			remove_node_internal(dnode);
			return true;
		}
	}
	
	private void remove_node_internal(DocumentNode dnode){
		DocumentNode child = dnode.first_child;
		while (child != null) {
			this.remove_node_internal(child);
			child = child.next;
		}
		dnode.first_child = null;
		dnode.last_child = null;
		if (dnode.previous != null) {
			dnode.previous.next = dnode.next;
		}
		if (dnode.next != null) {
			dnode.next.previous = dnode.previous;
		}
		if (dnode.parent != null){
			if (dnode.parent.first_child == dnode){
				dnode.parent.first_child = dnode.next;
			}
			if (dnode.parent.last_child == dnode){
				dnode.parent.last_child = dnode.previous;
			}
		}
		nodes.remove(dnode.uuid);
		this.node_removed(dnode.uuid);
	}
	
	public bool set_node_text(string node, string text){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return false; }
			dnode.text = text;
			this.node_modified(node);
			return true;
		}
	}
	
	public bool set_node_flag(string node, string flag, bool enabled){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return false; }
			dnode.flags.set_flag(flag,enabled);
			this.node_modified(node);
			return true;
		}
	}
	
	public bool remove_all_node_flags_with_prefix(string node, string prefix){
		lock (nodes) {
			var dnode = nodes.get(node);
			if (dnode == null) { return false; }
			foreach(string flag in dnode.flags.list_flags()){
				if (flag.has_prefix(prefix)) {
					dnode.flags.clear_flag(flag);
				}
			}
			this.node_modified(node);
			return true;
		}
	}
	
}

public class Night.Frontend.DocumentModel.DocumentNode : Object {
	public string uuid = GLib.Uuid.string_random();
	public DocumentNode? next = null;
	public DocumentNode? previous = null;
	public DocumentNode? parent = null;
	public DocumentNode? first_child = null;
	public DocumentNode? last_child = null;
	public string text;
	public Night.Interface.DocumentModel.NodeType node_type;
	public Night.Util.Flaglist flags = new Night.Util.Flaglist();
	
	public DocumentNode(Night.Interface.DocumentModel.NodeType node_type, string text = ""){
		this.node_type = node_type;
		this.text = text;
	}
	
	public string to_string() {
		string flagstring = "";
		flags.foreach((flag) => {
			flagstring += @" '$(flag.escape())'";
		});
		return @"[$node_type] $uuid \"$(text.escape())\"$flagstring";
	}

}	
