public class Night.Interface.Cache.Info : Object {

	//in unix utc !!!
	public int64 time_downloaded_utc { get; protected set; }
	public int64 expiry_date_utc { get; protected set; }
	public string uri { get; protected set; }
	public bool pinned { get; protected set; default=false; }
	
	public Info.just_downloaded(string uri, int64 cache_period_seconds){
		this.uri = uri;
		var now = new DateTime.now_utc();
		this.time_downloaded_utc = now.to_unix();
		this.expiry_date_utc = this.time_downloaded_utc+cache_period_seconds;
	}
	
	public Info(string uri, int64 time_downloaded_utc, int64 expiry_date_utc, bool pinned = false){
		this.uri = uri;
		this.time_downloaded_utc = time_downloaded_utc;
		this.expiry_date_utc = expiry_date_utc;
		this.pinned = pinned;
	}
	
	public bool is_expired(){
		var now = new DateTime.now_utc();
		return now.to_unix() > this.expiry_date_utc;
	}
	
}
