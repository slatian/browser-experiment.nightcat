public class Night.Interface.Cache.StoreReturn : Object {

	public string cache_id { get; protected set; }
	public Night.Interface.ResourceStream.Writer writer { get; protected set; }
	public Night.Interface.ResourceStream.Receiver? receiver { get; protected set; } //A receiver for reading the resource, that was just stored
	
	public StoreReturn(string cache_id, Night.Interface.ResourceStream.Writer writer, Night.Interface.ResourceStream.Receiver? receiver = null){
		this.cache_id = cache_id;
		this.writer = writer;
		this.receiver = receiver;
	}
}
