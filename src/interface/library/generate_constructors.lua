while true do
	local i = io.read()
	if not i then break end
	print("\tpublic LogEntry."..i.."(string context_id, ContextType context_type, string resource_id, string key, string val){")
	print("\t\tthis.log_entry_type = Night.Interface.Library.LogEntryType."..i:upper()..";")
	print("\t\tthis.context_id = context_id;")
	print("\t\tthis.context_type = context_type;")
	print("\t\tthis.resource_id = resource_id;")
	print("\t\tthis.key = key;")
	print("\t\tthis.val = val;")
	print("\t}")
	print("\t")
end
