public interface Night.Interface.Library.Librarian : Object {
	
	//only return after the request has finished
	public abstract void request(Night.Interface.Library.Context context);
	
}
