public interface Night.Interface.Library.Library : Object {
	
	//only return after the request has finished
	public abstract void request(Night.Interface.Library.CallbackHandler callback_handler, Night.Interface.Library.Verb verb, string uri, string reason);
	
}
