public enum Night.Interface.Library.LogEntryType {
	CONTEXT_OPENED,
	ACTION_PROVIDED,
	ACTION_UNAVAIABLE,
	ACTION_TRIGGERED,
	FEEDBACK,
	RESOURCE_WRITER_REQUESTED,
	RESOURCE_WRITER_DELIVERED,
	RESOURCE_WRITER_DENYED,
	RESOURCE_WRITER_CLOSED,
	RESOURCE_READER_REQUESTED,
	RESOURCE_READER_DELIVERED,
	RESOURCE_READER_DENYED,
	RESOURCE_READER_CLOSED,
	RESOURCE_FEEDBACK,
	CONTEXT_CLOSED_SUCCESS,
	CONTEXT_CLOSED_ERROR;

	public string to_string(){
		switch(this) {
			case CONTEXT_OPENED:
				return "context_opened";
			case ACTION_PROVIDED:
				return "action_provided";
			case ACTION_UNAVAIABLE:
				return "action_unavaiable";
			case ACTION_TRIGGERED:
				return "action_triggered";
			case FEEDBACK:
				return "feedback";
			case RESOURCE_WRITER_REQUESTED:
				return "resource_writer_requested";
			case RESOURCE_WRITER_DELIVERED:
				return "resource_writer_delivered";
			case RESOURCE_WRITER_DENYED:
				return "resource_writer_denyed";
			case RESOURCE_WRITER_CLOSED:
				return "resource_writer_closed";
			case RESOURCE_READER_REQUESTED:
				return "resource_reader_requested";
			case RESOURCE_READER_DELIVERED:
				return "resource_reader_delivered";
			case RESOURCE_READER_DENYED:
				return "resource_reader_denyed";
			case RESOURCE_READER_CLOSED:
				return "resource_reader_closed";
			case RESOURCE_FEEDBACK:
				return "resource_feedback";
			case CONTEXT_CLOSED_SUCCESS:
				return "context_closed_success";
			case CONTEXT_CLOSED_ERROR:
				return "context_closed_error";
			default:
				return "unknown";
		}
	}

	public static LogEntryType? from_string(string? name){
		if (name == null) { return null; }
		switch(name) {
			case "context_opened":
				return LogEntryType.CONTEXT_OPENED;
			case "action_provided":
				return LogEntryType.ACTION_PROVIDED;
			case "action_unavaiable":
				return LogEntryType.ACTION_UNAVAIABLE;
			case "action_triggered":
				return LogEntryType.ACTION_TRIGGERED;
			case "feedback":
				return LogEntryType.FEEDBACK;
			case "resource_writer_requested":
				return LogEntryType.RESOURCE_WRITER_REQUESTED;
			case "resource_writer_delivered":
				return LogEntryType.RESOURCE_WRITER_DELIVERED;
			case "resource_writer_denyed":
				return LogEntryType.RESOURCE_WRITER_DENYED;
			case "resource_writer_closed":
				return LogEntryType.RESOURCE_WRITER_CLOSED;
			case "resource_reader_requested":
				return LogEntryType.RESOURCE_READER_REQUESTED;
			case "resource_reader_delivered":
				return LogEntryType.RESOURCE_READER_DELIVERED;
			case "resource_reader_denyed":
				return LogEntryType.RESOURCE_READER_DENYED;
			case "resource_reader_closed":
				return LogEntryType.RESOURCE_READER_CLOSED;
			case "resource_feedback":
				return LogEntryType.RESOURCE_FEEDBACK;
			case "context_closed_success":
				return LogEntryType.CONTEXT_CLOSED_SUCCESS;
			case "context_closed_error":
				return LogEntryType.CONTEXT_CLOSED_ERROR;
			default:
				return null;
		}
	}

}
