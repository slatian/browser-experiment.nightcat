public enum Night.Interface.Library.Verb {
	DOWNLOAD,
	UPLOAD,
	DELETE,
	RELOAD;

	public string to_string(){
		switch(this) {
			case DOWNLOAD:
				return "download";
			case UPLOAD:
				return "upload";
			case DELETE:
				return "delete";
			case RELOAD:
				return "reload";
			default:
				return "unknown";
		}
	}

	public static Verb? from_string(string? name){
		if (name == null) { return null; }
		switch(name) {
			case "download":
				return Verb.DOWNLOAD;
			case "upload":
				return Verb.UPLOAD;
			case "delete":
				return Verb.DELETE;
			case "reload":
				return Verb.RELOAD;
			default:
				return null;
		}
	}

}
