public interface Night.Interface.Log.Logger : Object {
	public abstract void log(string name, string message, Night.Interface.Log.Severity severity);
}

public enum Night.Interface.Log.Severity {
	ERROR,
	WARNING,
	INFO,
	DEBUG;
	
	public string to_string(){
		switch(this){
			case Night.Interface.Log.Severity.ERROR:
				return "error";
			case Night.Interface.Log.Severity.WARNING:
				return "warning";
			case Night.Interface.Log.Severity.INFO:
				return "info";
			case Night.Interface.Log.Severity.DEBUG:
				return "debug";
			default:
				return "unknown";
		}
	}
}
