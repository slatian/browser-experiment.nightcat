
public delegate Night.Interface.Module.Module Night.Interface.Module.Initalize(string configuration, Night.Interface.Module.ObjectDictionary object_dictionary);


//should have a constructor that thakes a configuration string to make integration easier
//configuration should be in kv format, but may be in a different format if another format works better than kv
public interface Night.Interface.Module.Module : Night.Interface.Module.ObjectDictionary, Object{
	public abstract void shutdown();
	
	//sets this as the new configuration, everything set from the old cnfigurations must be reset to its default value, a module manager may restart the module if it doesn't support this operation
	public virtual bool reload_configuration(string new_configuration){ return false; }
	public abstract string get_configuration();
	
	//each module has some registers, that can hold strings, these are intended to be used for scripting and retreiving module status, they may also expose configuration options, that are subject to change
	public abstract void foreach_register(Func<string> cb);
	public abstract bool write_register(string register_id, string val);
	public abstract string? read_register(string register_id);
	
	//a status report should be in kv format and be a readout of all relevant registers with the register id as key and the register value as value
	//which registers are relevant is up to the module, a module may also include other information than registers, however generating a status report is usually an expensive operation so this shouldn't be the only place for this information
	public abstract string get_status_report();
	
	//returns the object dictionary this module was initalized with and will use for its lifetime
	public abstract Night.Interface.Module.ObjectDictionary get_object_dictionary();
}
