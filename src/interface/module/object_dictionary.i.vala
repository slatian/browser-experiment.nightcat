public interface Night.Interface.Module.ObjectDictionary : Object {
	public abstract void object_dictionary_foreach_name(Func<string> cb);
	public abstract Object? object_dictionary_get_object(string name);
}
