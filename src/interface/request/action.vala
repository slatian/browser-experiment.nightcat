public enum Night.Interface.Request.Action {
	DOWNLOAD,
	UPLOAD,
	DELETE,
	EVENTCONNECT;
	
	public string to_string(){
		switch(this) {
			case DOWNLOAD:
				return "download";
			case UPLOAD:
				return "upload";
			case DELETE:
				return "delete";
			case EVENTCONNECT:
				return "eventconnect";
			default:
				return "unknown";
		}
	}
	
	public static Action? from_string(string? action){
		if (action == null) { return null; }
		switch(action) {
			case "download":
				return Action.DOWNLOAD;
			case "upload":
				return Action.UPLOAD;
			case "delete":
				return Action.DELETE;
			case "eventconnect":
				return Action.EVENTCONNECT;
			default:
				return null;
		}
	}
}
