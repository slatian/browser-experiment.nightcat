//returns true if connection can be reused (check again in the connector, if it is reuseable on a socket level, before actually reusing it)
//all unrecoverable Errors thrown by the connection should be handled by the connector
public delegate bool Night.Interface.Request.ConnectorCallback(GLib.IOStream connection) throws Error;

public interface Night.Interface.Request.Connector : Object {

	public abstract string? request_connection(Night.Interface.Request.ContextInterface context);
	public abstract bool can_handle_address_type(string address_type);
	public abstract bool can_handle_configuration_key(string address, string key);
	
}
