//gets called to start an operation with a certin context, context gets closed when it returns (if not already closed), returns either the most significant error or null on success, return value gets ignored, if already closed
public delegate string? Night.Interface.Request.WorkerFunction(Night.Interface.Request.ContextInterface context);
//gets called when a connection was established, returns true if a connection could be reused (checks, if it hasn't closed etc. must be made by the one receiving the reurn value, wich is passed trough to the connector)
//all unrecoverable Errors thrown by the connection should be handled by the connector
public delegate bool Night.Interface.Request.ConnectionCallback(Night.Interface.Request.ContextInterface context, GLib.IOStream connection) throws Error;

public interface Night.Interface.Request.ContextInterface : Object {
	//introduce yourself
	
	public abstract bool initalize_interface(string name, Night.Interface.Request.ContextRole role);
	
	//get information about the interface
	
	public abstract bool is_open();
	public abstract bool close_requested();
	
	//get information about yourself
	
	public abstract string get_my_name();
	public abstract Night.Interface.Request.ContextRole get_my_role();
	public abstract string get_my_id();
	public abstract string? get_my_parents_id();
	
	//get information about your job
	public abstract Night.Interface.Request.ContextConfiguration get_my_configuration(); //also contains uri, action etc.
	public abstract Night.Interface.ResourceStream.Receiver? get_argument_resource();
	
	//get information about others
	
	public abstract Night.Interface.Request.Request get_request();
	
	//submit information 
	
	public abstract void submit_feedback(string key, string val);
	public abstract bool submit_connection(GLib.IOStream connection, bool always_close) throws Error; //returns true when the connection could be reused
	public abstract void set_most_significant_child(string? context_uuid);
	
	//close the context
	
	public abstract void close_because_of_success();
	public abstract void close_because_of_error(string most_significant_error);
	//will set the <most_significant_error> feedback value to "true" if it isn't already set
	
	//call for help
	
	//public abstract Night.Interface.Request.Request make_subrequest(string uri, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource, bool reload = false);
	public abstract bool open_connection(string address, Night.Interface.Request.ConnectionCallback callback, out string context_uuid = null, Night.Interface.Request.Connector? connector = null);
	public abstract string call_worker(Night.Interface.Request.WorkerFunction worker, string uri, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource = null, bool parallel = false, Night.Interface.Request.ResourceAllocator? allocation_callback = null, string? address = null); //if the parallel flag is set the worker will be run in a new threat
	public abstract Night.Interface.Request.ContextInterface create_context_interface(string uri, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource = null, Night.Interface.Request.ResourceAllocator? allocation_callback = null, string? address = null);
	
	public abstract Night.Interface.Request.NameResolverResult resolve_host(string scheme, string hostname, string port, Night.Interface.Request.NameResolver? resolver = null);
	public abstract Night.Interface.Request.NameResolverResult resolve_authority(string scheme, string authority, Night.Interface.Request.NameResolver? resolver = null);
	public abstract Night.Interface.Request.NameResolverResult resolve_urn(string urn, Night.Interface.Request.NameResolver? resolver = null);
	
	//intended to be used by name resolution multiplexers, so that you only have to write them once
	public abstract Night.Interface.Request.ContextInterface? create_resolver_subinterface();
	
	public abstract Night.Interface.Request.ResourceStreamOutput? allocate_logged_resource(string uri, Night.Interface.Request.ResourceAllocator? allocation_callback = null);
	
}
