public enum Night.Interface.Request.ContextRole {
	//actors
	REQUEST_ROUTER,
	RESOURCE_CACHE,
	FETCHER,
	//resolvers
	HOST_RESOLVER,
	AUTHORITY_RESOLVER,
	URN_RESOLVER,
	//transmissions
	DOWNLOAD,
	UPLOAD,
	DELETION,
	EVENT_CHANNEL,
	//connectors
	CONNECTION_ROUTER,
	//CONNECTION_CACHE, //Only activate when concept is finished
	CONNECTION_WRAPPER,
	CONNECTION_SOCKET;
	
	public Night.Interface.Request.ContextType get_context_type(){
		switch(this) {
			case URN_RESOLVER:
			case AUTHORITY_RESOLVER:
			case HOST_RESOLVER:
				return Night.Interface.Request.ContextType.RESOLVER;
			case DOWNLOAD:
			case UPLOAD:
			case DELETION:
			case EVENT_CHANNEL:
				return Night.Interface.Request.ContextType.TRANSMISSION;
			case CONNECTION_ROUTER:
			//case CONNECTION_CACHE:
			case CONNECTION_WRAPPER:
			case CONNECTION_SOCKET:
				return Night.Interface.Request.ContextType.CONNECTOR;
			case REQUEST_ROUTER:
			case RESOURCE_CACHE:
			case FETCHER:
			default:
				return Night.Interface.Request.ContextType.ACTOR;
		}
	}
	
	public string to_string(){
		switch(this) {
			case DOWNLOAD:
				return "download";
			case UPLOAD:
				return "upload";
			case DELETION:
				return "deletion";
			case EVENT_CHANNEL:
				return "event_channel";
			case CONNECTION_ROUTER:
				return "connection_router";
			//case CONNECTION_CACHE:
			//	return "connection_cache";
			case CONNECTION_WRAPPER:
				return "connection_wrapper";
			case CONNECTION_SOCKET:
				return "connection_socket";
			case REQUEST_ROUTER:
				return "request_router";
			case RESOURCE_CACHE:
				return "resource_cache";
			case FETCHER:
				return "fetcher";
			case HOST_RESOLVER:
				return "host_resolver";
			case AUTHORITY_RESOLVER:
				return "authority_resolver";
			case URN_RESOLVER:
				return "urn_resolver";
			default:
				return "unknown";
		}
	}
	
}
