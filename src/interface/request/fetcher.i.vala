public interface Night.Interface.Request.Fetcher : Object {
	
	public abstract bool can_handle_uri(string uri, Night.Interface.Request.Action action);
	public abstract string? request(Night.Interface.Request.ContextInterface context);
	
	//Note the fetcher has to register its context before the request function returns, if not the request is considered failed
	
}
