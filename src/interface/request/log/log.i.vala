public interface Night.Interface.Request.Log.Log : Object {

	public abstract Night.Interface.Request.Log.Entry? get_entry(uint32 n);
	public abstract uint32 get_n_entrys();
	
	public signal void entry_added(uint32 n, Night.Interface.Request.Log.Entry entry);
	
	public abstract bool records_debug_messages();
	public abstract void foreach(Func<Night.Interface.Request.Log.Entry> cb); //iterates over all entrys in chronological order
	
}
