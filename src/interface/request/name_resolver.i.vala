public interface Night.Interface.Request.NameResolverResult : Object {
	
	//if the next function returns null this means there are no entrys left and the resolver will close automatically, the next function will block for the time the lookup takes
	public abstract string? next(out Night.Interface.Request.NameCategory category = null, out int rating = null);
	public abstract void close();
	public abstract bool is_closed();
	public virtual Night.Interface.Request.NameCategory? peek_next_category(){ return null; }
	
}

public interface Night.Interface.Request.NameResolver : Object {

	// configuration must contain a name key (resolvers must be able to handle the name not being there!)
	// authority resolver: scheme,authority
	// urn resolver: urn
	// host resolver: scheme,hostname,port
	
	//depsite the name this really should do a lazy lookup, meaning it gathers all configuration, but really does the real lookup with the first next() call.
	public abstract Night.Interface.Request.NameResolverResult lookup(Night.Interface.Request.ContextInterface context);
	
}

public enum Night.Interface.Request.NameCategory {
	MOST_PREFERRED = 4,
	PREFERRED = 3,
	NEUTRAL = 2,
	DISCOURAGED = 1,
	AVOID = 0;
	
	public string to_string(){
		switch(this){
			case MOST_PREFERRED:
				return "most_preferred";
			case PREFERRED:
				return "preferred";
			case NEUTRAL:
				return "neutral";
			case DISCOURAGED:
				return "discouraged";
			case AVOID:
				return "avoid";
			default:
				return "unknown";
		}
	}
	
}
