public interface Night.Interface.Request.RequestContext : Object {
	//Instances of this interface are given to the session as request context
	public abstract void set_request_object(Night.Interface.Request.Request request);
	
	public abstract Night.Interface.ResourceStream.Writer? request_resource_download_destination(string context_uuid, string uri);
	
	public abstract void append_log_entry(Night.Interface.Request.Log.Entry entry);
	
}
