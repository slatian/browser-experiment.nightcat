public interface Night.Interface.Request.ResourceStreamOutput : Object {
	public abstract Night.Interface.ResourceStream.Writer get_writer();
	
	public abstract void mark_as_success();
	public abstract void mark_as_errored(string most_significant_error);
	
}
