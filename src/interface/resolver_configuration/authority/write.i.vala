public interface Night.Interface.ResolverConfiguration.Authority.Write : Object {
	public abstract bool add_alias(string from_scheme, string to_scheme);
	public abstract bool remove_alias(string from_scheme, string to_scheme);
	
	public abstract string? add_rule(string scheme_template, string to_scheme_template, string default_port, string? port = null, string? host_template = null, Night.Interface.Request.NameCategory category = Night.Interface.Request.NameCategory.NEUTRAL);
	public abstract bool remove_rule(string uuid);
}

//Some of the above functions don't take objects to enforce static tables, that can only be changed trough official interfaces
