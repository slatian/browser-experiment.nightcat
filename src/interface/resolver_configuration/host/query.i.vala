public interface Night.Interface.ResolverConfiguration.Host.Query : Object {
	public abstract void foreach_alias(string? scheme, Func<Night.Interface.ResolverConfiguration.Alias> cb);
	public abstract void foreach_rule(string? scheme, Func<Night.Interface.ResolverConfiguration.Host.Rule> cb);
	public abstract void foreach_resolver(string? group_name, Func<Night.Interface.ResolverConfiguration.Host.Resolver> cb);
}
