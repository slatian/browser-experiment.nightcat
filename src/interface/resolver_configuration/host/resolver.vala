public class Night.Interface.ResolverConfiguration.Host.Resolver : Object{
	public string group_name { get; protected set; }
	public string resolver_name { get; protected set; }
	public Night.Interface.Request.NameCategory category { get; protected set; }
	public Night.Interface.Request.NameResolver resolver { get; protected set; }
	
	public Resolver(string group_name, string resolver_name, Night.Interface.Request.NameCategory category, Night.Interface.Request.NameResolver resolver){
		this.group_name = group_name;
		this.resolver_name = resolver_name;
		this.category = category;
		this.resolver = resolver;
	}
	
}
