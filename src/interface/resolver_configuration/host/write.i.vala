public interface Night.Interface.ResolverConfiguration.Host.Write : Object {
	public abstract bool add_alias(string from_scheme, string to_scheme);
	public abstract bool remove_alias(string from_scheme, string to_scheme);
	
	public abstract bool set_resolver(string resolver_name, string group_name, Night.Interface.Request.NameCategory category, Night.Interface.Request.NameResolver resolver); //only one resolver for each name
	public abstract bool remove_resolver(string resolver_name);
	public abstract Night.Interface.Request.NameResolver? get_resolver(string resolver_name);
	
	public abstract string? add_rule(string scheme_template, string resolver_group, string? host_template = null, Night.Interface.Request.NameCategory? override_category = null);
	public abstract bool remove_rule(string uuid);
}

//Some of the above functions don't take objects to enforce static tables, that can only be changed trough official interfaces
