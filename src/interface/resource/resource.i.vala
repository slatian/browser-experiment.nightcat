public interface Night.Interface.Resource.Resource : Object {

	public abstract string get_resource_uid();

	//modes can be set from resource writer
	public abstract Night.Interface.Resource.Mode get_mode();
	public signal void mode_updated(Resource emitter);
	
	public abstract string? get_attribute(string key);
	public abstract uint64 get_current_size();
	public abstract bool exists(); //will return false after resource is 
	public abstract Night.Util.Eventbus? get_eventbus(); // returns the eventbus used for this resource
	
	//will return the local url for the resource, if it's stored in a shard location
	//(A file:// url for linux machines, potentially a data:// url for small content)
	//This may only be used for reading!
	public abstract string? get_local_url();
	
	public signal void size_updated(Resource emitter);
	public signal void eventbus_set(Resource emitter);
	public signal void attribute_updated(Night.Interface.Resource.Resource emitter, string key);
	
	public abstract Night.Interface.Resource.Reader get_reader();
}

public enum Night.Interface.Resource.Mode {
	DELETED = 0,
	STATIC = 1,
	APPENDABLE = 2,
	UPDATABLE = 3;
	
	public string to_string(){
		switch(this){
			case DELETED:
				return "deleted";
			case STATIC:
				return "static";
			case APPENDABLE:
				return "appendable";
			case UPDATABLE:
				return "updateable";
			default:
				return "unknown";
		}
	}	
	
	public bool writable(){
		switch(this){
			case APPENDABLE:
			case UPDATABLE:
				return true;
			case STATIC:
			case DELETED:
			default:
				return false;
		}
	}
	
}
