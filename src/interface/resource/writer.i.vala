public interface Night.Interface.Resource.Writer : Object {
	public abstract bool clear(Cancellable? cancellable = null); //returns true if resource was cleared
	public abstract bool append(uint8[] data, Cancellable? cancellable = null); //returns true if successful
	public abstract bool commit(Cancellable? cancellable = null); //returns true if resource is writable and and the changes were applied
	public abstract void reset(); //resets the writer to a nop commit
	
	//convenience function, may also be used as an optimization if no other clears and appends are staged
	public virtual bool append_and_commit(uint8[] data, Cancellable? cancellable = null){
		if (!append(data, cancellable)) { return false; }
		if (!commit(cancellable)) {
			reset();
			return false;		
		}
		return true;
	}
	
	public abstract bool delete_resource();
	
	public abstract bool writable();
	public abstract bool updatable();
	public abstract bool can_commit(); //If the current commit still meets the restrictions of the resource
	public abstract bool is_closed();
	
	//the following functions are applyed immedeantly and do not require a commit
	public abstract bool close(); //closes the writer
	public abstract bool downgrade_append_only(); //downgrades the resource from an updatable to an appendable one
	public abstract bool downgrade_read_only(); //turns off the ability to write to the resource
	
	public abstract bool set_eventbus(Night.Util.Eventbus eventbus);
	public abstract bool set_attribute(string key, string val);
	
	public abstract Night.Interface.Resource.Resource get_resource();
}
