public class Night.Interface.ResourceStream.ResourceReceiver : Night.Interface.ResourceStream.Receiver, Object {
	
	private Night.Interface.Resource.Resource resource;
	
	public ResourceReceiver(Night.Interface.Resource.Resource resource){
		this.resource = resource;
		this.resource.attribute_updated.connect(on_attribute_set);
	}
	
	~ResourceReceiver(){
		this.resource.attribute_updated.disconnect(on_attribute_set);
	}
	
	private void on_attribute_set(Night.Interface.Resource.Resource emitter, string key){
		this.attribute_set(key, resource.get_attribute(key));
	}
	
	  ///////////////////////////////////////////////
	 //  Night.Interface.ResourceStream.Receiver  //
	///////////////////////////////////////////////
	
	public string? get_attribute(string key){
		return resource.get_attribute(key);
	}
	
	public Night.Interface.ResourceStream.Reader open_reader(){
		return resource.get_reader();
	}
	
}
