public interface Night.Interface.Session.Session : Object {
	
	public abstract string get_session_uuid();
	public abstract bool set_fetcher(Night.Interface.Request.Fetcher? fetcher, string? scheme = null);
	public abstract bool set_connector(Night.Interface.Request.Connector connector);
	
	public abstract Night.Interface.Request.Request request_download(Night.Interface.Request.RequestContext context, string uri, bool reload=false);
	public abstract Night.Interface.Request.Request request_upload(Night.Interface.Request.RequestContext context, string uri, Night.Interface.ResourceStream.Receiver resource);
	public abstract Night.Interface.Request.Request request_deletion(Night.Interface.Request.RequestContext context, string uri);
	public abstract Night.Interface.Request.Request request_event_channel(Night.Interface.Request.RequestContext context, string uri);
	
	public virtual void erase_cache(){}
	
	//Note: these will be used for both urns and socket adresses
	public virtual Night.Interface.Request.NameCategory categorize_address(string address){
		return Night.Interface.Request.NameCategory.NEUTRAL;
	}
	
	public virtual int rate_address(string address){
		return 0;
	}
	
	public virtual Night.Interface.Request.NameCategory categorize_uri(string address){
		return Night.Interface.Request.NameCategory.NEUTRAL;
	}
	
	public virtual int rate_uri(string address){
		return 0;
	}
	
	public abstract void set_name(string name);
	public abstract string get_name();
	
}
